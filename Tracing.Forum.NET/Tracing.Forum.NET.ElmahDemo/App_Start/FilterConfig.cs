﻿using System.Web;
using System.Web.Mvc;
using Elmah.Contrib.WebApi;

namespace Tracing.Forum.NET.ElmahDemo
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

        }
    }
}
