﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Elmah;

namespace Tracing.Forum.NET.ElmahDemo.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        [Route("SimpleAction")]
        public string SimpleAction()
        {
            throw new NotImplementedException("You actually have to code your actions!");
        }

        [HttpGet]
        [Route("ExceptionalCode")]
        public string ExceptionalCode()
        {
            var returnVal = "This code works perfectly";
            try
            {
                throw new NotSupportedException("Where'd it go?");
            }
            catch (NotSupportedException e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                /*
                 * ELMAH will use the current HttpContext in this scenario above
                 * 
                 * However, ELMAH supports non-web scenarios, and in this case you can use:
                 * ErrorLog.GetDefault(null).Log(new Error(e));
                 */
                 
                returnVal = e.Message;
            }

            return returnVal;
        }
    }
}
