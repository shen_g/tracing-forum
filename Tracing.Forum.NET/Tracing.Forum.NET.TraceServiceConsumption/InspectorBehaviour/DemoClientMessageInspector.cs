﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Tracing.Forum.NET.TraceServiceConsumption.InspectorBehaviour
{
    public class DemoClientMessageInspector : IClientMessageInspector
    {
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            Debug.WriteLine(request.ToString());
            return null;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            Debug.WriteLine(reply.ToString());
        }
    }
}
