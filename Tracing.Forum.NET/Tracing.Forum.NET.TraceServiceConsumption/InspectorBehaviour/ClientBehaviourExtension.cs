﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Tracing.Forum.NET.TraceServiceConsumption.InspectorBehaviour
{
    public class ClientBehaviourExtension : BehaviorExtensionElement
    {
        protected override object CreateBehavior()
        {
            return new DemoClientEndpointBehaviour();
        }

        public override Type BehaviorType
        {
            get { return typeof (DemoClientEndpointBehaviour); } 
        }
    }
}
