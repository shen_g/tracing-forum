﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracing.Forum.NET.TraceServiceConsumption
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var myRandomSvc = new ServiceReference1.Service1Client())
            {
                var result = myRandomSvc.GetData(1);
            }
            Console.ReadLine();
        }
    }
}
