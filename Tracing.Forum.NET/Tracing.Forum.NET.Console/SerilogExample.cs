﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace Tracing.Forum.NET.Console
{
    class SerilogExample
    {
        private ILogger _logger;

        public SerilogExample()
        {
            _logger = new LoggerConfiguration().WriteTo.RollingFile("serilog-file.txt").MinimumLevel.Verbose().CreateLogger();
        }

        public void RunExample()
        {
            System.Console.WriteLine("About to write debug message - press enter to continue");
            System.Console.ReadLine();
            _logger.Debug("Serilog - debug message");

            System.Console.WriteLine("About to write verbose message - press enter to continue");
            System.Console.ReadLine();
            _logger.Verbose("Serilog - verbose message");

            System.Console.WriteLine("About to write info message - press enter to continue");
            System.Console.ReadLine();
            _logger.Information("Serilog - info message");

            System.Console.WriteLine("About to write warn message - press enter to continue");
            System.Console.ReadLine();
            _logger.Warning("Serilog - warn message");

            System.Console.WriteLine("About to write error message - press enter to continue");
            System.Console.ReadLine();
            _logger.Error("Serilog - error message");

            System.Console.ReadLine();
        }
    }
}
