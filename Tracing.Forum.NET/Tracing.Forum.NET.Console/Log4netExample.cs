﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Tracing.Forum.NET.Console
{
    class Log4netExample
    {
        private ILog _logger;

        public Log4netExample()
        {
            log4net.Config.XmlConfigurator.Configure();
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        public void RunExample()
        {
            System.Console.WriteLine("About to write debug message - press enter to continue");
            System.Console.ReadLine();
            _logger.Debug("log4net - debug message");

            System.Console.WriteLine("About to write info message - press enter to continue");
            System.Console.ReadLine();
            _logger.Info("log4net - info message");

            System.Console.WriteLine("About to write warn message - press enter to continue");
            System.Console.ReadLine();
            _logger.Warn("log4net - warn message");

            System.Console.WriteLine("About to write error message - press enter to continue");
            System.Console.ReadLine();
            _logger.Error("log4net - error message");

            System.Console.ReadLine();
        }
    }
}
