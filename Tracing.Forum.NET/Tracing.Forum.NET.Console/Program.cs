﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracing.Forum.NET.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            System.Console.WriteLine("log4net example");
            var log4netExample = new Log4netExample();
            log4netExample.RunExample();

            System.Console.WriteLine("NLog example");
            var nlogExample = new NLogExample();
            nlogExample.RunExample();
            */
            System.Console.WriteLine("Serilog example");
            var serilogExample = new SerilogExample();
            serilogExample.RunExample();
        }
    }
}
