﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Tracing.Forum.NET.Console
{
    class NLogExample
    {
        private Logger _logger;

        public NLogExample()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public void RunExample()
        {
            System.Console.WriteLine("About to write debug message - press enter to continue");
            System.Console.ReadLine();
            _logger.Debug("NLog - debug message");

            System.Console.WriteLine("About to write trace message - press enter to continue");
            System.Console.ReadLine();
            _logger.Debug("NLog - trace message");

            System.Console.WriteLine("About to write info message - press enter to continue");
            System.Console.ReadLine();
            _logger.Info("NLog - info message");

            System.Console.WriteLine("About to write warn message - press enter to continue");
            System.Console.ReadLine();
            _logger.Warn("NLog - warn message");

            System.Console.WriteLine("About to write fatal message - press enter to continue");
            System.Console.ReadLine();
            _logger.Fatal("NLog - fatal message");

            System.Console.WriteLine("About to write error message - press enter to continue");
            System.Console.ReadLine();
            _logger.Error("NLog - error message");

            System.Console.ReadLine();
        }
    }
}
